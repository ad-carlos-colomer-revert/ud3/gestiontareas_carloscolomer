/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import controllerBD.ConexionBD;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import modelo.Empleado;
import static modelo.Empleado.empleadoLog;

/**
 *
 * @author carlos
 */
public class empleadoDAO implements GenericoDAO {

    private EntityManager em;

    public empleadoDAO() {

        try {
            em = ConexionBD.getConnection();
        } catch (SQLException ex) {
            System.err.println("Error conectadose a la base de datos empleadoDAO " + ex);
        }
    }

    @Override
    public Object findByPK(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List findAll() throws Exception {
        TypedQuery<Empleado> query = em.createQuery("SELECT p FROM Empleado p", Empleado.class);
        List<Empleado> results = query.getResultList();
        for (Empleado p : results) {
//            System.out.println(results.size());
//
//            System.out.println(p);
        }
        return results;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Object t) throws Exception {
        Empleado e = (Empleado) t;
        em.getTransaction().begin();
        em.persist(e);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        Empleado a = (Empleado) t;
        boolean update;

        em.getTransaction().begin();

        em.getTransaction().commit();

        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        TypedQuery<Empleado> query = em.createQuery("SELECT p FROM Empleado p", Empleado.class);
        List<Empleado> results = query.getResultList();
        em.getTransaction().begin();

        for (Empleado empleadoRemove : results) {
            em.remove(empleadoRemove);
            System.out.println("Borrado " + empleadoRemove);
        }
        em.getTransaction().commit();

        return true;
    }

    public boolean login(String nombre, String apellido, String passwd) {
        boolean correcto = false;
        TypedQuery<Empleado> query = em.createQuery("SELECT p FROM Empleado p where p.nombre = :name and p.apellidos = :apell and p.passwd= :contra", Empleado.class)
                .setParameter("name", nombre)
                .setParameter("apell", apellido)
                .setParameter("contra", passwd);
        List<Empleado> results = query.getResultList();
        if (results.size() > 0) {
            empleadoLog = results.get(0);
            correcto = true;
        }

        return correcto;
    }

    public boolean comprobarExistencia(String nombre, String apellido) {
        boolean correcto = false;
        TypedQuery<Empleado> query = em.createQuery("SELECT p FROM Empleado p where p.nombre = :name and p.apellidos = :apell", Empleado.class)
                .setParameter("name", nombre)
                .setParameter("apell", apellido);

        List<Empleado> results = query.getResultList();
        if (results.size() > 0) {

            correcto = true;
        }

        return correcto;
    }

}
