/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import controllerBD.ConexionBD;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import modelo.Tarea;
import static modelo.Empleado.empleadoLog;
import static modelo.Tarea.tareaSeleccionada;

/**
 *
 * @author carlos
 */
public class tareaDAO implements GenericoDAO {

    private EntityManager em;

    public tareaDAO() {

        try {
            em = ConexionBD.getConnection();
        } catch (SQLException ex) {
            System.err.println("Error conectadose a la base de datos empleadoDAO " + ex);
        }
    }

    @Override
    public Tarea findByPK(int id) throws Exception {
        Tarea tarea = null;
        TypedQuery<Tarea> query = em.createQuery("SELECT p FROM Tarea p where id = " + id, Tarea.class);
        List<Tarea> results = query.getResultList();
        em.getTransaction().begin();

        for (Tarea tareas : results) {
            tarea = tareas;
   
        }
        em.getTransaction().commit();

        return tarea;
    }

    @Override
    public List findAll() throws Exception {
        TypedQuery<Tarea> query = em.createQuery("SELECT p FROM Tarea p where encargados.id=" + empleadoLog.getId(), Tarea.class);
        List<Tarea> results = query.getResultList();
//        for (Tarea p : results) {
//            System.out.println(results.size());
//
//            System.out.println(p);
//        }
        return results;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Object t) throws Exception {
        Tarea tarea = (Tarea) t;
        em.getTransaction().begin();
        em.persist(tarea);
        em.getTransaction().commit();

        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        Tarea a = (Tarea) t;
        boolean update;
        em.getTransaction().begin();

        em.getTransaction().commit();

        return true;
    }

    public void addSubtarea(Tarea tareaSeleccionada, Tarea tarea) {
        em.getTransaction().begin();
        System.out.println("addSubTarea TareaDAO " + tarea.toString());
        tareaSeleccionada.setSubTareas(tarea);
        em.getTransaction().commit();
    }

    @Override
    public boolean delete(int id) throws Exception {
        TypedQuery<Tarea> query = em.createQuery("SELECT p FROM Tarea p where id = " + id, Tarea.class);
        List<Tarea> results = query.getResultList();
        em.getTransaction().begin();

        for (Tarea tareaRemove : results) {
            em.remove(tareaRemove);
            System.out.println("Borrado " + tareaRemove);
        }
        em.getTransaction().commit();

        return true;
    }

    // Borro las tareas con encargados, ya que las que no tienen encargados son subtareas
    public boolean deleteCompleted() throws Exception {
        boolean eliminado;
        ArrayList<Tarea> subtareasID = null;
        TypedQuery<Tarea> query = em.createQuery("SELECT p FROM Tarea p where realizada = true and encargados is not null", Tarea.class);
        List<Tarea> results = query.getResultList();
        em.getTransaction().begin();

        for (Tarea tareaRemove : results) {
            if (!tareaRemove.getSubTareas().isEmpty() && tareaRemove.getSubTareas().size() > 0){
                System.out.println("Tiene subTarea " + tareaRemove.getSubTareas().toString());
                //Esto da error 
           //     subtareasID.addAll(tareaRemove.getSubTareas());
            }
            em.remove(tareaRemove);
            System.out.println("Borrado " + tareaRemove);
        }
        em.getTransaction().commit();
        
     //   deleteSubTask(subtareasID);

        return true;
    }

    public boolean deleteSubTaskCompleted() throws Exception {
        System.out.println("deleteSubTaskCompleted " + tareaSeleccionada.getId());
        boolean eliminado;
        //Obtenemos el id de las tareas que cumplen el requisito del la sentencia
        TypedQuery<Integer> query = em.createQuery("SELECT p.subTareas.id FROM Tarea p where subTareas.realizada = true and id = " + tareaSeleccionada.getId(), Integer.class);
        List<Integer> results = query.getResultList();
        em.getTransaction().begin();

        for (Integer tareaRemove : results) {
            //Enviamos los IDs de las subtareas a removeSubTask, y nos devuelve el objeto correspondiente y lo eliminamos de la BD
            em.remove(tareaSeleccionada.removeSubTask(tareaRemove));
            
            System.out.println("Borrado " + tareaRemove);
        }
        em.getTransaction().commit();

        return true;
    }
    
    public boolean deleteSubTask(List<Tarea> tareas) throws Exception {
        System.out.println("deleteSubTaskComplete " + tareaSeleccionada.getId());
        boolean eliminado;
        for(Tarea tarea : tareas){
            System.out.println("deleteSubTaks" + tarea.toString());
            
//             //Obtenemos el id de las tareas que cumplen el requisito del la sentencia
//        TypedQuery<Integer> query = em.createQuery("SELECT p.subTareas.id FROM Tarea p where id = " + i, Integer.class);
//        List<Integer> results = query.getResultList();
//        em.getTransaction().begin();
//
//        for (Integer tareaRemove : results) {
//            //Enviamos los IDs de las subtareas a removeSubTask, y nos devuelve el objeto correspondiente y lo eliminamos de la BD
//            em.remove(tareaSeleccionada.removeSubTask(tareaRemove));
//            System.out.println("Borrado " + tareaRemove);
//        }
//        em.getTransaction().commit();
        
    }
       

        return true;
    }

}
