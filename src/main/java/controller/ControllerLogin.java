/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import carlos.gestareas_carloscolomer.App;
import dao.empleadoDAO;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.stage.Stage;
import modelo.Empleado;
import static modelo.Empleado.empleadoLog;

/**
 *
 * @author carlos
 */
public class ControllerLogin implements Initializable {

    @FXML
    private Button btnEntrar;
    @FXML
    private Button btnRegistrarse;
    @FXML
    private TextField tfUsuario;
    @FXML
    private PasswordField tfPasswd;

    private empleadoDAO eDAO;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        tfUsuario.setEditable(true);
        tfPasswd.setEditable(true);
        eDAO = new empleadoDAO();

    }

    @FXML
    private void actionEntrar() {
        boolean correcto = false;

        try {
            String contra = tfPasswd.getText();
            String passwd = "";
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(contra.getBytes("utf8"));
            passwd = String.format("%040x", new BigInteger(1, digest.digest()));

            String user = tfUsuario.getText();
            String[] partsUser = user.split("-");

            if (partsUser.length == 2) {
                
                correcto = eDAO.login(partsUser[0], partsUser[1], passwd);
                
            } 
            if (correcto) {

                FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("ViewPrincipalEmpl.fxml"));
                Stage stage = new Stage();
                stage.setScene(new Scene(fxmlLoader.load()));
                stage.show();
                cerrarVentana(btnEntrar);

            } else {
                Alert errorAlert = new Alert(AlertType.ERROR);
                errorAlert.setHeaderText("El usuario y/o la contraseña son erroneas.");
                errorAlert.setContentText("Por favor, vuelve a intentarlo.");
                errorAlert.showAndWait();
            }

        } catch (IOException ex) {
            mensajeExcepcion(ex, "Error abriendo la ventana principal");
        } catch (NoSuchAlgorithmException ex) {
            mensajeExcepcion(ex, "Error en el cifrado de la contraseña");
        }
    }

    @FXML
    private void actionRegistrarse() {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("ViewRegistro.fxml"));
            Stage stage = new Stage();
            stage.setScene(new Scene(fxmlLoader.load()));
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(ControllerLogin.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
//

    private void mensajeExcepcion(Exception ex, String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error de excepción");
        alert.setHeaderText(msg);
        alert.setContentText(ex.getMessage());

        String exceptionText = "";
        StackTraceElement[] stackTrace = ex.getStackTrace();
        for (StackTraceElement ste : stackTrace) {
            exceptionText = exceptionText + ste.toString() + System.getProperty("line.separator");
        }

        Label label = new Label("La traza de la excepción ha sido: ");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    private void cerrarVentana(Button click) {
        Stage stage = (Stage) click.getScene().getWindow();

        stage.close();
    }

}
