/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.tareaDAO;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import modelo.Tarea;
import static modelo.Tarea.tareaSeleccionada;
import static modelo.Empleado.empleadoLog;

/**
 *
 * @author carlos
 */
public class ControllerSubTarea implements Initializable {

    @FXML
    private Label lbDescripcion;
    @FXML
    private Label lbFechaInicio;
    @FXML
    private Button btnAdd, btnCancelar;
    @FXML
    private Label lbFechaFin;
    @FXML
    private DatePicker dpFechaAlta, dpFechaFin;
    @FXML
    private TextField tfDescripcion;
    @FXML
    private CheckBox realizada;
    @FXML
    private TextArea taObservaciones;
    @FXML
    private ListView<Tarea> lvSubTareas;

    private tareaDAO tareaDAO;
    private Tarea seleccionada;
    private boolean fechaCorrecta;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        tareaDAO = new tareaDAO();
        lbDescripcion.setText(tareaSeleccionada.getDescripcion());
        lbFechaFin.setText(tareaSeleccionada.getFechaFinalizacion().toString());
        lbFechaInicio.setText(tareaSeleccionada.getFechaAlta().toString());
        lvSubTareas.setOnMouseClicked(e -> accionSeleccionTabla(e));
        listarSubtareas();
        deshabilitarFunciones();

    }

    @FXML
    private void actionAdd() {
        try {
            if (btnAdd.getText().equals("Añadir")) {
                LocalDate FechaAlta = dpFechaAlta.getValue();
                LocalDate FechaFin = dpFechaFin.getValue();
                Date dateAlta = Date.valueOf(FechaAlta);
                Date dateFin = Date.valueOf(FechaFin);

                comprobarFechas(dateAlta, dateFin);

                if (fechaCorrecta) {
                    System.out.println("coerrto");
                    Tarea tarea = new Tarea(dateAlta, dateFin, tfDescripcion.getText());

                    tareaDAO.insert(tarea);
                    tareaDAO.addSubtarea(tareaSeleccionada, tarea);
                    lvSubTareas.getItems().add(tarea);
                    deshabilitarFunciones();
                }

            } else {
                System.out.println("Hola guardar");
                LocalDate FechaAlta = dpFechaAlta.getValue();
                LocalDate FechaFin = dpFechaFin.getValue();
                Date dateAlta = Date.valueOf(FechaAlta);
                Date dateFin = Date.valueOf(FechaFin);

                comprobarFechas(dateAlta, dateFin);

                if (fechaCorrecta) {
                    System.out.println("actualizo");
                    seleccionada.setFechaAlta(dateAlta);
                    seleccionada.setFechaFinalizacion(dateFin);
                    seleccionada.setDescripcion(tfDescripcion.getText());
                    seleccionada.setObservaciones(taObservaciones.getText());
                    if (realizada.isSelected()) {
                        seleccionada.setRealizada(true);
                    }
                    tareaDAO.update(seleccionada);
                    lvSubTareas.refresh();

                    deshabilitarFunciones();
                }

            }

        } catch (Exception ex) {
            mensajeExcepcion(ex, "Error añadiendo/modificando la subtarea");
        }

    }

    @FXML
    private void actionCancelar() {
        if (btnCancelar.getText().equals("Cancelar")) {
            deshabilitarFunciones();
        } else {
            try {
                
                System.out.println("Eliminar");
                tareaDAO.deleteSubTaskCompleted();

                listarSubtareas();
            } catch (Exception ex) {
                mensajeExcepcion(ex, "Error eliminando las subtareas completadas.");
            }
        }

    }

    private void accionSeleccionTabla(javafx.scene.input.MouseEvent e) {
        if (e.getClickCount() == 2) {
            habilitarFunciones();
            seleccionada = (Tarea) lvSubTareas.getSelectionModel().getSelectedItem();
            dpFechaAlta.setValue(seleccionada.getFechaAlta().toLocalDate());
            dpFechaFin.setValue(seleccionada.getFechaFinalizacion().toLocalDate());
            tfDescripcion.setText(seleccionada.getDescripcion());
            taObservaciones.setText(seleccionada.getObservaciones());
            realizada.setSelected(seleccionada.isRealizada());

            tareaSeleccionada = seleccionada;

        }
    }

    private void habilitarFunciones() {

        btnCancelar.setText("Cancelar");
        btnAdd.setText("Guardar");
        taObservaciones.setDisable(false);
        realizada.setDisable(false);
    }

    private void listarSubtareas() {
        try {
            System.out.println("listaSubTareas" + tareaSeleccionada.getId());
            Tarea tarea = tareaDAO.findByPK(tareaSeleccionada.getId());
            
            lvSubTareas.getItems().clear();
            
        System.out.println(tareaSeleccionada.getSubTareas());
        if (tarea.getSubTareas()!=null && !tarea.getSubTareas().isEmpty() && tarea.getSubTareas().size() > 0) {
            lvSubTareas.getItems().addAll(tarea.getSubTareas());
        }
        } catch (Exception ex) {
            mensajeExcepcion(ex, "Error obteniendo la lista de subTareas. Vuelve a intentarlo");
        }

    }

    private void deshabilitarFunciones() {

        btnCancelar.setText("Eliminar subtareas completadas");
        realizada.setDisable(true);
        taObservaciones.setDisable(true);
        btnAdd.setText("Añadir");
        taObservaciones.setText("");
        tfDescripcion.setText("");
        realizada.setSelected(false);
        dpFechaAlta.setValue(null);
        dpFechaFin.setValue(null);

    }

    private void comprobarFechas(Date dateAlta, Date dateFin) {
        if (dateFin.before(dateAlta)) {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("La fehca de fin no puede ser anterior a la de inicio");
            errorAlert.setContentText("Por favor, vuelve a intentarlo.");
            errorAlert.showAndWait();
            fechaCorrecta = false;
        } else {
            fechaCorrecta = true;
        }
    }

    private void mensajeExcepcion(Exception ex, String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error de excepción");
        alert.setHeaderText(msg);
        alert.setContentText(ex.getMessage());

        String exceptionText = "";
        StackTraceElement[] stackTrace = ex.getStackTrace();
        for (StackTraceElement ste : stackTrace) {
            exceptionText = exceptionText + ste.toString() + System.getProperty("line.separator");
        }

        Label label = new Label("La traza de la excepción ha sido: ");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

}
