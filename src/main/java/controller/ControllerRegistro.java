/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.empleadoDAO;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import modelo.Empleado;

/**
 *
 * @author carlos
 */
public class ControllerRegistro implements Initializable {

    @FXML
    private Button btnRegistro;
    @FXML
    private Button btnCancelar;
    @FXML
    private TextField tfNombre;
    @FXML
    private TextField tfApellidos;
    @FXML
    private TextField tfPasswd;

    private empleadoDAO eDAO;
    private boolean existe;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        eDAO = new empleadoDAO();

    }

    @FXML
    private void actionRegistrar() {
        boolean correcto = false;

        try {
            String passwd = tfPasswd.getText();
            String sha1 = "";
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(passwd.getBytes("utf8"));
            sha1 = String.format("%040x", new BigInteger(1, digest.digest()));

            Empleado e = new Empleado(tfNombre.getText(), tfApellidos.getText(), sha1);

            existe = eDAO.comprobarExistencia(tfNombre.getText(), tfApellidos.getText());

            if (existe) {
                Alert infoAlert = new Alert(AlertType.ERROR);
                infoAlert.setHeaderText("Usuario ya existente");
                infoAlert.setContentText("Por favor, intentelo de nuevo");
                infoAlert.showAndWait();
               

            } else {
                correcto = eDAO.insert(e);

                if (correcto) {
                    Alert infoAlert = new Alert(AlertType.INFORMATION);
                    infoAlert.setHeaderText("Usuario creado correctamente");
                    infoAlert.setContentText("Por favor, recuerde que su nombre de ususario es " + tfNombre.getText() + "-" + tfApellidos.getText());
                    infoAlert.showAndWait();
                    Stage stage = (Stage) btnRegistro.getScene().getWindow();

                    stage.close();
                }

            }

        } catch (Exception ex) {
            mensajeExcepcion(ex, "Error al registrar el usuario");
        }

    }

    @FXML
    private void actionCancelar() {

        Stage stage = (Stage) btnCancelar.getScene().getWindow();

        stage.close();

    }

    private void mensajeExcepcion(Exception ex, String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error de excepción");
        alert.setHeaderText(msg);
        alert.setContentText(ex.getMessage());

        String exceptionText = "";
        StackTraceElement[] stackTrace = ex.getStackTrace();
        for (StackTraceElement ste : stackTrace) {
            exceptionText = exceptionText + ste.toString() + System.getProperty("line.separator");
        }

        Label label = new Label("La traza de la excepción ha sido: ");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

}
