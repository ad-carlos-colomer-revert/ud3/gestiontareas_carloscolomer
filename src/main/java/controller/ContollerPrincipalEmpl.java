/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import carlos.gestareas_carloscolomer.App;
import dao.tareaDAO;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.sql.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import modelo.Tarea;
import static modelo.Empleado.empleadoLog;
import static modelo.Tarea.tareaSeleccionada;

/**
 *
 * @author carlos
 */
public class ContollerPrincipalEmpl implements Initializable {

    @FXML
    private Button btnOpenTask;
    @FXML
    private Button btnAdd;
    @FXML
    private Button btnCompartir;
    @FXML
    private Button btnCancel;
    @FXML
    private Label lbUsuario;
    @FXML
    private ListView<Tarea> lvTareas;
    @FXML
    private DatePicker dpFechaAlta, dpFechaFin;
    @FXML
    private TextField tfDescripcion;
    @FXML
    private CheckBox realizada;
    @FXML
    private TextArea taObservaciones;

    private tareaDAO tareaDAO;
    private Tarea seleccionada;
    private boolean fechaCorrecta;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {

        
        lbUsuario.setText(empleadoLog.getNombre());
        tareaDAO = new tareaDAO();
        lvTareas.setOnMouseClicked(e -> accionSeleccionTabla(e));
        deshabilitarFunciones();
        listarTareas();

    }

    @FXML
    private void actionAdd() {
        try {
            if (btnCompartir.isDisabled()) { //insertar
                LocalDate FechaAlta = dpFechaAlta.getValue();
                LocalDate FechaFin = dpFechaFin.getValue();
                Date dateAlta = Date.valueOf(FechaAlta);
                Date dateFin = Date.valueOf(FechaFin);

                comprobarFechas(dateAlta, dateFin);

                if (fechaCorrecta) {
                    Tarea tarea = new Tarea(dateAlta, dateFin, tfDescripcion.getText(), empleadoLog);

                    tareaDAO.insert(tarea);
                    lvTareas.getItems().add(tarea);
                    deshabilitarFunciones();
                }

            } else {    //update
                System.out.println("Hola guardar ");
                LocalDate FechaAlta = dpFechaAlta.getValue();
                LocalDate FechaFin = dpFechaFin.getValue();
                Date dateAlta = Date.valueOf(FechaAlta);
                Date dateFin = Date.valueOf(FechaFin);

                comprobarFechas(dateAlta, dateFin);

                if (fechaCorrecta) {

                    seleccionada.setFechaAlta(dateAlta);
                    seleccionada.setFechaFinalizacion(dateFin);
                    seleccionada.setDescripcion(tfDescripcion.getText());
                    seleccionada.setObservaciones(taObservaciones.getText());
                    if (realizada.isSelected()) {
                        seleccionada.setRealizada(true);
                    }

                    tareaDAO.update(seleccionada);
                    lvTareas.refresh();

                    deshabilitarFunciones();

                }

            }

        } catch (Exception ex) {
            mensajeExcepcion(ex, "Error añadiendo la tarea");
        }
    }

    @FXML
    private void actionOpenTask() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("ViewSubTarea.fxml"));
            Stage stage = new Stage();
            stage.setScene(new Scene(fxmlLoader.load()));
            stage.show();
        } catch (IOException ex) {
            mensajeExcepcion(ex, "Error abriendo las tarea");
        }
    }

    @FXML
    private void actionCompartir() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("ViewListaEmplados.fxml"));
            Stage stage = new Stage();
            stage.setScene(new Scene(fxmlLoader.load()));
            stage.showAndWait();
        } catch (IOException ex) {
            mensajeExcepcion(ex, "Error abriendo los empleados");
        }

        listarTareas();
    }

    @FXML
    private void actionCancelar() {
        boolean eliminado;
        if(btnCancel.getText().equals("Cancelar")){
            deshabilitarFunciones();
        }else{
            try {
                System.out.println("Eliminar");
                tareaDAO.deleteCompleted();
                listarTareas();
            } catch (Exception ex) {
                mensajeExcepcion(ex, "Error eliminando las tareas completadas.");
            }
        }
        
    }

    private void accionSeleccionTabla(javafx.scene.input.MouseEvent e) {
        if (e.getClickCount() == 2) {
            habilitarFunciones();
            seleccionada = (Tarea) lvTareas.getSelectionModel().getSelectedItem();
            dpFechaAlta.setValue(seleccionada.getFechaAlta().toLocalDate());
            dpFechaFin.setValue(seleccionada.getFechaFinalizacion().toLocalDate());
            tfDescripcion.setText(seleccionada.getDescripcion());
            taObservaciones.setText(seleccionada.getObservaciones());
            realizada.setSelected(seleccionada.isRealizada());

            tareaSeleccionada = seleccionada;

        }
    }

    private void listarTareas() {

        try {
            lvTareas.getItems().clear();
            List<Tarea> tareas = tareaDAO.findAll();

            for (Tarea tarea : tareas) {

                lvTareas.getItems().add(tarea);

            }
        } catch (Exception ex) {
            mensajeExcepcion(ex, "Error listando las tareas");
        }

    }

    private void deshabilitarFunciones() {
        btnCompartir.setDisable(true);
        btnOpenTask.setDisable(true);
        realizada.setDisable(true);
        taObservaciones.setDisable(true);
        btnAdd.setText("Añadir");
        btnCancel.setText("Eliminar tareas completadas");
        taObservaciones.setText("");
        tfDescripcion.setText("");
        realizada.setSelected(false);
        dpFechaAlta.setValue(null);
        dpFechaFin.setValue(null);

    }

    private void habilitarFunciones() {
        btnCancel.setText("Cancelar");
        btnCompartir.setDisable(false);
        btnOpenTask.setDisable(false);
        btnAdd.setText("Guardar");
        taObservaciones.setDisable(false);
        realizada.setDisable(false);
    }

    private void comprobarFechas(Date dateAlta, Date dateFin) {
        if (dateFin.before(dateAlta)) {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("La fehca de fin no puede ser anterior a la de inicio");
            errorAlert.setContentText("Por favor, vuelve a intentarlo.");
            errorAlert.showAndWait();
            fechaCorrecta = false;
        } else {
            fechaCorrecta = true;
        }

    }

    private void mensajeExcepcion(Exception ex, String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error de excepción");
        alert.setHeaderText(msg);
        alert.setContentText(ex.getMessage());

        String exceptionText = "";
        StackTraceElement[] stackTrace = ex.getStackTrace();
        for (StackTraceElement ste : stackTrace) {
            exceptionText = exceptionText + ste.toString() + System.getProperty("line.separator");
        }

        Label label = new Label("La traza de la excepción ha sido: ");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

}
