/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.empleadoDAO;
import dao.tareaDAO;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import modelo.Empleado;
import static modelo.Tarea.tareaSeleccionada;

/**
 *
 * @author carlos
 */
public class ControllerListaEmpleados implements Initializable {

    @FXML
    private Button btnCancelar, btnAceptar;
    @FXML
    private ListView<Empleado> lvTodosLosEmpleados;
    @FXML
    private ListView<Empleado> lvEmpleadosAsignados;

    private empleadoDAO empleadoDAO;
    private Empleado seleccionado;
    private ArrayList<Empleado> empleadosAsignados;
    private tareaDAO tareaDAO;
    private boolean hayEmpleadosAsignados;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        empleadoDAO = new empleadoDAO();
        empleadosAsignados = tareaSeleccionada.getEngargados();
        tareaDAO = new tareaDAO();
        lvTodosLosEmpleados.setOnMouseClicked((e -> accionSeleccionTabla(e)));
        lvEmpleadosAsignados.setOnMouseClicked((e -> actionRemoveEmpleado(e)));

        cargarEmpleados();

    }

    @FXML
    private void actionAceptar() {
        try {

            comprobarEmplados();
            
            if (hayEmpleadosAsignados) {

                tareaSeleccionada.setEngargados(empleadosAsignados);
                tareaDAO.update(tareaSeleccionada);

                Stage stage = (Stage) btnAceptar.getScene().getWindow();

                stage.close();

            } else {

                Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                errorAlert.setHeaderText("No hay empleados asignados a la tarea");
                errorAlert.setContentText("Por favor, vuelve a intentarlo.");
                errorAlert.showAndWait();

            }

        } catch (Exception ex) {
            mensajeExcepcion(ex, "Error actualizando los encargados de la tarea");
        }

    }

    @FXML
    private void actionCancelar() {
        Stage stage = (Stage) btnCancelar.getScene().getWindow();

        stage.close();
    }

    @FXML
    private void actionAddEmpleado() {

        if (seleccionado != null) {
            lvEmpleadosAsignados.getItems().add(seleccionado);
            lvTodosLosEmpleados.getItems().remove(seleccionado);
            empleadosAsignados.add(seleccionado);
            seleccionado = null;
        } else {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Selecciona el empleado que desea asignar a la tarea.");
            errorAlert.setContentText("Por favor, vuelve a intentarlo.");
            errorAlert.showAndWait();
        }

    }

    @FXML
    private void actionRemoveEmpleado() {

        if (seleccionado != null) {
            lvEmpleadosAsignados.getItems().remove(seleccionado);
            lvTodosLosEmpleados.getItems().add(seleccionado);
            empleadosAsignados.remove(seleccionado);
            seleccionado = null;
        } else {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Selecciona el empleado que desea eliminar de la tarea.");
            errorAlert.setContentText("Por favor, vuelve a intentarlo.");
            errorAlert.showAndWait();
        }

    }

    private void accionSeleccionTabla(javafx.scene.input.MouseEvent e) {

            seleccionado = (Empleado) lvTodosLosEmpleados.getSelectionModel().getSelectedItem();
            
            
    }

    private void actionRemoveEmpleado(javafx.scene.input.MouseEvent e) {

        seleccionado = (Empleado) lvEmpleadosAsignados.getSelectionModel().getSelectedItem();

    }

    private void cargarEmpleados() {
        try {
            List<Empleado> empleados = empleadoDAO.findAll();
            List<Empleado> empleadosYaAsignados = tareaSeleccionada.getEngargados();

            for (Empleado empleado : empleados) {
                for (Empleado empl : empleadosYaAsignados) {
                    if (empleado.equals(empl)) {
                        lvEmpleadosAsignados.getItems().add(empleado);
                    }
                }
                if (!lvEmpleadosAsignados.getItems().contains(empleado)) {
                    lvTodosLosEmpleados.getItems().add(empleado);
                }

            }
        } catch (Exception ex) {
            mensajeExcepcion(ex, "Error listando los empleados");
        }

    }

    private void mensajeExcepcion(Exception ex, String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error de excepción");
        alert.setHeaderText(msg);
        alert.setContentText(ex.getMessage());

        String exceptionText = "";
        StackTraceElement[] stackTrace = ex.getStackTrace();
        for (StackTraceElement ste : stackTrace) {
            exceptionText = exceptionText + ste.toString() + System.getProperty("line.separator");
        }

        Label label = new Label("La traza de la excepción ha sido: ");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    private void comprobarEmplados() {

        if (empleadosAsignados.isEmpty()) {
            hayEmpleadosAsignados = false;
        } else {
            hayEmpleadosAsignados = true;
        }
    }

}
