/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author carlos
 */
@Entity
public class Empleado implements Serializable {

     
     
    
    @Id
    @GeneratedValue
    @OneToMany(cascade=CascadeType.REMOVE)
    private int id;
    private String nombre;
    private String apellidos;
   // private ArrayList<Tarea> listaTareas;
    private String passwd;
    public static Empleado empleadoLog;
    
    public Empleado(){
        
    }

//    public Empleado(String nombre, String apellidos, String passwd, ArrayList<Tarea> listaTareas) {
//        this.nombre = nombre;
//        this.passwd = passwd;
//        this.apellidos = apellidos;
//        this.listaTareas = listaTareas;
//    }
    public Empleado(String nombre, String apellidos, String passwd) {
        this.nombre = nombre;
        this.passwd = passwd;
        this.apellidos = apellidos;
  
    }
    
//    public void addTarea(Tarea tarea){
//        this.listaTareas.add(tarea);
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

//    public ArrayList<Tarea> getListaTareas() {
//        return listaTareas;
//    }
//
//    public void setListaTareas(ArrayList<Tarea> listaTareas) {
//        this.listaTareas = listaTareas;
//    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Override
    public String toString() {
        return  id + " - " + nombre + " " + apellidos;
    }
    
    
    
}
