/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.sql.Date;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import static modelo.Empleado.empleadoLog;

/**
 *
 * @author carlos
 */
@Entity
public class Tarea implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    private Date fechaAlta;
    private Date fechaFinalizacion;
    private String descripcion;
    private ArrayList<Tarea> subTareas;
    private boolean realizada;
    private String observaciones;
    private ArrayList<Empleado> encargados;
    public static Tarea tareaSeleccionada;

    public Tarea() {

    }

    public Tarea(Date fechaAlta, Date fechaFinalizacion, String descripcion, ArrayList<Tarea> subTareas,
            boolean realizada, String observaciones, ArrayList<Empleado> encargados) {
        this.fechaAlta = fechaAlta;
        this.fechaFinalizacion = fechaFinalizacion;
        this.descripcion = descripcion;
        this.subTareas = subTareas;
        this.realizada = realizada;
        this.observaciones = observaciones;
        this.encargados = encargados;
    }

    public Tarea(Date fechaAlta, Date fechaFinalizacion, String descripcion, boolean realizada, String observaciones, ArrayList<Empleado> encargados) {
        this.fechaAlta = fechaAlta;
        this.fechaFinalizacion = fechaFinalizacion;
        this.descripcion = descripcion;
        this.realizada = realizada;
        this.observaciones = observaciones;
        this.encargados = encargados;
    }

    public Tarea(Date fechaAlta, Date fechaFinalizacion, String descripcion, Empleado encargado) {

        this.fechaAlta = fechaAlta;
        this.fechaFinalizacion = fechaFinalizacion;
        this.descripcion = descripcion;
        this.encargados = new ArrayList<>();
        this.encargados.add(encargado);
        this.subTareas = new ArrayList<>();
    }

    public Tarea(Date fechaAlta, Date fechaFinalizacion, String descripcion) {

        this.fechaAlta = fechaAlta;
        this.fechaFinalizacion = fechaFinalizacion;
        this.descripcion = descripcion;

    }

    public void compartirTarea(Empleado encargado) {
        this.encargados.add(encargado);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaFinalizacion() {
        return fechaFinalizacion;
    }

    public void setFechaFinalizacion(Date fechaFinalizacion) {
        this.fechaFinalizacion = fechaFinalizacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<Tarea> getSubTareas() {
        return subTareas;
    }

    public Tarea removeSubTask(int subTareaRemove) {
        Tarea tareaRemove = null;
        for (Tarea tarea : subTareas) {
            if (tarea.getId() == subTareaRemove) {
                //Cuando obtenemos la subtarea que queremos eliminar, a asignamos a la variable
                tareaRemove = tarea;

            }
        }
        // Eliminamos la subtarea de la arraylist de subtareas
        this.subTareas.remove(tareaRemove);

        return tareaRemove;
    }
    
    public void removeAllSubTask(){
        this.subTareas.clear();
//        int total = subTareas.size();
//        for(int i = 0; i<total; i++){
//            System.out.println("Elimino " + i);
//            this.subTareas.remove(i);
//        }

    }

    public void setSubTareas(Tarea subTareas) {
        this.subTareas.add(subTareas);

    }

    public boolean isRealizada() {
        return realizada;
    }

    public void setRealizada(boolean realizada) {
        this.realizada = realizada;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public ArrayList<Empleado> getEngargados() {
        return encargados;
    }

    public void setEngargados(ArrayList<Empleado> encargados) {
        this.encargados = encargados;
    }

    @Override
    public String toString() {
        return "Fecha de alta: " + fechaAlta + ", Fecha fin: " + fechaFinalizacion + "\nDescripción: " + descripcion + "\nRealizada: " + realizada;
    }

}
